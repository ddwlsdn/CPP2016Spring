#include <iostream>

using namespace std;

class IntegerSet {
	int numElements, maxNumElements;
	int *elements;
public:
	IntegerSet(int max) {
		numElements = 0;
		maxNumElements = max;
		elements = new int[max];
	}
	~IntegerSet();
	int addAnElement(int); // add an integer element, to implement
	IntegerSet operator+(const IntegerSet&); // union, to implement
	IntegerSet operator-(const IntegerSet&); // difference, to implement
	IntegerSet operator*(const IntegerSet&); // intersection, to implement
	bool operator&(int);
	friend const ostream& operator<<(const ostream&, const IntegerSet&); // given as below
};

const ostream& operator<<(const ostream& osIntegerSet, const IntegerSet& integerSet) {
	cout << "number of elements : " << integerSet.numElements << endl;
	for (int i = 0; i < integerSet.numElements; i++) {
		cout << integerSet.elements[i] << " ";
	}
	cout << endl;

	return osIntegerSet;
}

int main() {
	int nums[10] = { 3, 7, 1, 6, 9, 2, 4, 5, 10 };
	IntegerSet integerSet(10);

	for (int i = 0; i < 10; i++) {
		integerSet.addAnElement(nums[i]);
	}

	bool isContains = integerSet & 11;
	if (isContains) {
		cout << "TRUE" << endl;
	}
	else  {
		cout << "FALSE" << endl;
	}

	return EXIT_SUCCESS;
}

