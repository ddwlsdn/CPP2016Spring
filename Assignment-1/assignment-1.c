#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int insert(char** editor, int arrLen) {

	char *curEditor = *editor;
	char* newEditor;
	char* temp;

	int index;				// 삽입할 시작 인덱스
	char buffer[BUFSIZ];	// 입력 받을 문자열 버퍼
	
	/* 문자열 길이 */
	int strLen = strlen(curEditor);
	int newLength;
	int bufStrLen = 0;

	int i;

	/* 삽입할 문자열을 집어넣을 인덱스 입력 */
	do {
		printf("삽입할 문자열의 시작 인덱스를 입력하세요(0 ~ %d) : ", strLen);
		
		scanf("%d", &index);
		getchar();

		if (index < 0 || index > strLen) {
			printf("잘못된 입력입니다.\n");
		} else {
			break;
		}
	} while (1);

	/* 삽입할 문자열 입력*/
	printf("문자열을 입력하세요(최대 %d 자) : ", BUFSIZ);
	scanf("%s", &buffer);
	bufStrLen = strlen(buffer);

	/* 새 문자열 길이 계산 */
	newLength = strLen + bufStrLen;

	/* [실습 문제] 배열의 크기 이상으로 입력된 경우 Exception 출력 후 문자열 삽입 종료*/
	/*
	if (newLength >= arrLen) {
		printf("에디터에 삽입될 문자열의 크기가 배열의 크기보다 큽니다.\n");
		return arrLen;
	}
	*/

	/* 조정된 문자열 길이에 맞게 배열의 길이 조정 */
	while (newLength + 1 > arrLen) {
		arrLen *= 2;
	}
	/* 코드의 가독성을 위해 삽입마다 할당 (원래 이러면 안되유...) */
	newEditor = (char*)malloc(sizeof(char) * arrLen);

	/* 삽입할 인덱스 이전의 문자열을 옮기기 */
	for (i = 0; i < index; i++) {
		newEditor[i] = curEditor[i];
	}

	/* 삽입할 인덱스 뒤의 문자열을 옮기기 */
	for (i = index; i < strLen; i++) {
		newEditor[bufStrLen + i] = curEditor[i];
	}

	/* 삽입된 문자열 복사 */
	for (i = 0; i < bufStrLen; i++) {
		newEditor[index + i] = buffer[i];
	}

	newEditor[newLength] = '\0';

	//할당 해제
	temp = curEditor;
	curEditor = newEditor;
	free(temp);

	*editor = curEditor;

	return arrLen;
}

int erase(char** editor, int arrLen) {
	int index;
	int length;

	/* 문자열 길이 */
	int strLen = strlen(*editor);

	int i;

	/* 삭제할 문자열의 시작 위치 인덱스 입력 */
	do {
		printf("삭제할 문자열의 시작 인덱스를 입력하세요(0 ~ %d) : ", strLen - 1);

		scanf("%d", &index);
		getchar();

		if (index < 0 || index > strLen - 1) {
			printf("잘못된 입력입니다.\n");
		}
		else {
			break;
		}
	} while (1);

	printf("삭제할 문자열의 길이를 입력하세요 : ");
	scanf("%d", &length);

	/* 최대로 삭제할 수 있는 길이로 조정 */
	if (index + length > strLen) {
		length = strLen - index;
	}

	for (i = index; i < strlen(editor); i++) {
		(*editor)[i] = (*editor)[length + i];
	}

	(*editor)[strLen - length] = '\0';

	/* (추가문제) 문자열이 배열의 반 이하인 경우 배열 크기 재할당 */
	if (strLen + 1 < arrLen / 2) {
		while (strLen + 1 < arrLen / 2) {
			arrLen /= 2;
		}
		*editor = realloc(*editor, arrLen);
	}

	return arrLen;
}

int menu() {
	const int SECCESS = 1;
	int cmd = 0, scanResult;

	printf("1.입력\n2.삭제\n3.종료\n");

	scanResult = scanf("%d", &cmd);
	if (scanResult == SECCESS) {
		return cmd;
	}
	else {
		return 0;
	}
}

int operation(int cmd, char** editor, int arrLen) {
	switch (cmd) {
	case 1:
		arrLen = insert(editor, arrLen);
		break;
	case 2:
		arrLen = erase(editor, arrLen);
		break;
	case 3:
		exit(EXIT_SUCCESS);
	}

	printf("%s\n문자열 길이 : %d\n에디터 길이 : %d\n", *editor, strlen(*editor), arrLen);

	return arrLen;
}

int main(int argc, char* argv[]) {
	int cmd, arrLen;
	char* editor;

	if (argc != 2) {
		fputs("에러! 옵션을 입력하지 않으셨군요...\n", stderr);
		exit(EXIT_FAILURE);
	}

	/* 초기 배열의 크기 (10의 여유 크기) */
	arrLen = (strlen(argv[1]) + 1) + 10;

	/* 동적 메모리 할당, 문자열 복사 */
	editor = (char*)malloc(sizeof(char) * (arrLen));
	strcpy(editor, argv[1]);
	editor[strlen(argv[1])] = '\0';

	printf("%s\n문자열 길이 : %d\n에디터 길이 : %d\n", editor, strlen(editor), arrLen);

	do {
		cmd = menu();
		if (cmd != 0) {
			arrLen = operation(cmd, &editor, arrLen);
		}
	} while (cmd != 0);

	/* 메모리 할당 해제 */
	free(editor);

	return EXIT_SUCCESS;
}