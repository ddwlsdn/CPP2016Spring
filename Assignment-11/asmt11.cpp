#include <iostream>

int main() {
	ShapeSet shapeSet(20);

	// Rectangle
	for (int i = 0; i < 5; i++) {
		//Point leftLower(i * 2, 0);
		//Point rightUpper(i * 2 + 2, 2);
		//Rectangle rectangle(leftLower, rightUpper);

		Point* leftLower = new Point(i * 2, 0);
		Point* rightUpper = new Point(i * 2 + 2, 2);
		Rectangle* rectangle = new Rectangle(*leftLower, *rightUpper);

		shapeSet = shapeSet + (*rectangle);
	}

	// Total area : 20~
	cout << "Total area : " << shapeSet.totalArea() << endl;

	// Circle
	for (int i = 0; i < 5; i++) {
		//Point center(i * 2 + 1, 3);
		//double radius = 1;
		//Circle circle(center, radius);

		Point* center = new Point(i * 2 + 1, 3);
		double radius = 1;
		Circle* circle = new Circle(*center, radius);

		shapeSet = shapeSet + (*circle);
	}

	// Total area : 20 + 5PI
	cout << "Total area : " << shapeSet.totalArea() << endl;

	// Traingle
	for (int i = 0; i < 5; i++) {
		//Point triangleP1(i * 2, 4);
		//Point triangleP2(i * 2 + 1, 6);
		//Point triangleP3(i * 2 + 2, 4);
		//Triangle triangle(triangleP1, triangleP2, triangleP3);

		Point* triangleP1 = new Point(i * 2, 4);
		Point* triangleP2 = new Point(i * 2 + 1, 6);
		Point* triangleP3 = new Point(i * 2 + 2, 4);
		Triangle* triangle = new Triangle(*triangleP1, *triangleP2, *triangleP3);

		shapeSet = shapeSet + (*triangle);
	}

	// Total area : 45.7~
	cout << "Total area : " << shapeSet.totalArea() << endl;

	// duplication
	Point rectP1(0, 0);
	Point rectP2(2, 2);
	Rectangle rect(rectP1, rectP2);

	shapeSet = shapeSet + rect;
	cout << "Total area : " << shapeSet.totalArea() << endl;

	// Total area : 41.7~
	shapeSet = shapeSet - rect;
	cout << "Total area : " << shapeSet.totalArea() << endl;

	// Search
	for (int i = 0; i < 3; i++) {
		// (1,1), (3,3), (5,5)
		Point searchPoint(i * 2 + 1, i * 2 + 1);
		Shape& result = shapeSet.search(searchPoint);

		// 4, PI, 2
		cout << result.area() << endl;
	}

	return EXIT_SUCCESS;
}