/**
* You can eliminate POSIX deprecation warnings by defining _CRT_NONSTDC_NO_DEPRECATE.
*/
#ifdef _WIN32
#include <io.h>
#else
#include <unistd.h>
#endif

#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define AGE_NUM 2
#define ID_NUM 9
#define SCORE_NUM 4
#define MAX_NAME 40

typedef struct {
	int age;
	int id;
	float score;
	char name[MAX_NAME];
} Student;

void printStudents(const char* title, Student *students, int n) {
	int i;

	printf("======== %s ========\n", title);
	for (i = 0; i < n; i++) {
		printf("%d %d %f %s\n", students[i].age, students[i].id, students[i].score, students[i].name);
	}
	puts("================\n");
}

int readStudents(char* filename, Student **students) {

	int fd;
	char buffer[BUFSIZ];
	int readedBytes;

	int age, id;
	float score;
	char name[MAX_NAME];

	char ch;
	int idx;

	int arrLen = 10;
	int std_idx = 0;

	Student* studentArr = (Student*)malloc(sizeof(Student) * arrLen);
	Student* newStudentArr;

	fd = open(filename, O_RDONLY);

	do {
		//Read age value
		memset(buffer, 0, BUFSIZ);
		read(fd, buffer, AGE_NUM);
		age = atoi(buffer);
		
		//Read blank
		lseek(fd, 1, SEEK_CUR);

		//Read ID value
		memset(buffer, 0, BUFSIZ);
		read(fd, buffer, ID_NUM);
		id = atoi(buffer);

		//Read blank
		lseek(fd, 1, SEEK_CUR);

		//Read score value
		memset(buffer, 0, BUFSIZ);
		read(fd, buffer, SCORE_NUM);
		score = atof(buffer);

		//Read blank
		lseek(fd, 1, SEEK_CUR);

		//Read name value
		memset(buffer, 0, BUFSIZ);
		memset(name, 0, MAX_NAME);
		idx = 0;
		do {
			readedBytes = read(fd, &ch, 1);
			if (readedBytes) {
				if (ch != '\n') {
					buffer[idx] = ch;
					idx++;
				}
				else {
					break;
				}
			}
		} while (readedBytes);
		strcpy(name, buffer);

		//Store values to Student structure
		(studentArr[std_idx]).age = age;
		(studentArr[std_idx]).id = id;
		(studentArr[std_idx]).score = score;
		strcpy((studentArr[std_idx]).name, name);

		std_idx++;

		//Doubling array size 
		//if the number of stored value is more than size of array. 
		//@see Assignment-1 solution code 2016-03-09
		if (std_idx >= arrLen) {
			arrLen *= 2;

			newStudentArr = malloc(sizeof(Student) * arrLen);
			memcpy(newStudentArr, studentArr, sizeof(Student) * std_idx);
			free(studentArr);
			studentArr = newStudentArr;
		}
		
	} while (readedBytes);

	*students = studentArr;

	close(fd);

	return std_idx;
}

int compareStudent(const void * first, const void * second)
{
	int result;
	Student* firstStudent = (Student*)first;
	Student* secondStudent = (Student*)second;

	int firstId = firstStudent->id;
	int secondId = secondStudent->id;

	if (firstId < secondId) {
		result = -1;
	}
	else if (firstId > secondId) {
		result = 1;
	}
	else {
		result = 0;
	}
	return result;
}

void sortStudents(int n, Student *students) {
	qsort(students, n, sizeof(Student), compareStudent);
}

void writeStudents(char* filename, int n, Student *students) {
	int fd;
	//int i;
	fd = open(filename, O_WRONLY | O_CREAT | O_BINARY);

	write(fd, students, sizeof(Student) * n);
	/*
	for (i = 0; i < n; i++) {
		//write(fd, &(students[i]), sizeof(Student));

		//write(fd, &(students[i].age), sizeof(int));
		//write(fd, &(students[i].id), sizeof(int));
		//write(fd, &(students[i].score), sizeof(float));
		//write(fd, students[i].name, sizeof(char) * MAX_NAME);
	}
	*/

	close(fd);
}

void readKthStudent(char* filename, int k, Student *student) {
	int fd;
	int eof;
	int idx;
	
	if (k > 0) {
		fd = open(filename, O_RDONLY | O_BINARY);
		idx = sizeof(Student) * (k - 1);
		eof = lseek(fd, 0, SEEK_END);

		if (eof <= idx) {
			idx = eof - sizeof(Student);
		}

		lseek(fd, idx, SEEK_SET);
		read(fd, student, sizeof(Student));

		/*
		read(fd, &(student->age), sizeof(int));
		read(fd, &(student->id), sizeof(int));
		read(fd, &(student->score), sizeof(float));
		read(fd, student->name, sizeof(char) * MAX_NAME);
		*/

		close(fd);
	}
}

void writeKthStudent(char* filename, int k, Student *student) {
		int fd;
		int eof;
		int idx;

		if (k > 0) {
			fd = open(filename, O_WRONLY | O_BINARY);
			idx = sizeof(Student) * (k - 1);
			eof = lseek(fd, 0, SEEK_END);

			if (eof <= idx) {
				idx = eof - sizeof(Student);
			}

			lseek(fd, idx, SEEK_SET);
			write(fd, student, sizeof(Student));

			/*
			read(fd, &(student->age), sizeof(int));
			read(fd, &(student->id), sizeof(int));
			read(fd, &(student->score), sizeof(float));
			read(fd, student->name, sizeof(char) * MAX_NAME);
			*/

			close(fd);
		}
}

int readKthNStudent(char* filename, int k, Student *student, int n) {
	int fd, eof, idx, studentsSize;

	if (k > 0) {
		fd = open(filename, O_RDONLY | O_BINARY);

		idx = sizeof(Student) * (k - 1);
		eof = lseek(fd, 0, SEEK_END);
		studentsSize = sizeof(Student) * n;

		if (eof <= idx) {
			idx = eof - sizeof(Student);
		}
		lseek(fd, idx, SEEK_SET);

		if (eof - idx < studentsSize) {
			studentsSize = eof - idx;
			studentsSize /= sizeof(Student);
			studentsSize *= sizeof(Student);
		}
		read(fd, student, studentsSize);

		/*
			for ...
			read(fd, &(student->age), sizeof(int));
			read(fd, &(student->id), sizeof(int));
			read(fd, &(student->score), sizeof(float));
			read(fd, student->name, sizeof(char) * MAX_NAME);
		*/

		close(fd);
	}

	return (studentsSize / sizeof(Student));
}

void updateStudent(char *filename, int k, Student* student) {
	//read from Kth
	readKthStudent(filename, k, student);

	//Update Student value
	printStudents("Readed Student", student, 1);
	puts("Type values to update");
	scanf("%d %d %f %[^\n]", &(student->age), &(student->id), &(student->score), student->name);
	printStudents("Updated Student", student, 1);

	//write to Kth
	writeKthStudent(filename, k, student);
}

int main() {

	Student* students = NULL;
	Student readStudent, upStudent;

	Student* readNStudent;

	int studentCnt, readStudentCnt;
	int k, n;

	//Read
	studentCnt = readStudents("student.txt", &students);
	printStudents("After Reading from File", students, studentCnt);

	//Sort
	sortStudents(studentCnt, students);
	printStudents("After Sorting Students by id value", students, studentCnt);

	//Write
	writeStudents("result.bin", studentCnt, students);

	
	//Read Kth
	printf("input k : ");
	scanf("%d", &k);
	readKthStudent("result.bin", k, &readStudent);
	printStudents("After Read Kth Student", &readStudent, 1);

	//Read N Student from Kth
	printf("input k and n : ");
	scanf("%d %d", &k, &n);
	readNStudent = (Student*)malloc(sizeof(Student) * n);
	readStudentCnt = readKthNStudent("result.bin", k, readNStudent, n);
	printStudents("After Read N Students from Kth", readNStudent, readStudentCnt);
	

	//Update Kth Student
	printf("input k : ");
	scanf("%d", &k);
	updateStudent("result.bin", k, &upStudent);

	//Check whether updated
	readKthStudent("result.bin", k, &readStudent);
	printStudents("After Updating Student", &readStudent, 1);

	free(readNStudent);

	if (students != NULL) {
		free(students);
	}

	return EXIT_SUCCESS;
}