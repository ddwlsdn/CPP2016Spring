#include <iostream>

using namespace std;

class IntegerSet {
	int numElements, maxNumElements;
	int *elements;
public:
	IntegerSet(int max) {
		numElements = 0;
		maxNumElements = max;
		elements = new int[max];
	}
	~IntegerSet();
	int addAnElement(int); // add an integer element, to implement
	IntegerSet operator+(const IntegerSet&); // union, to implement
	IntegerSet operator-(const IntegerSet&); // difference, to implement
	IntegerSet operator*(const IntegerSet&); // intersection, to implement
	int operator~();
	friend const ostream& operator<<(const ostream&, const IntegerSet&); // given as below

	bool operator<(const IntegerSet&);
	bool operator==(const IntegerSet&);
	IntegerSet& operator=(const IntegerSet&);
	int operator[](int k);
	friend istream& operator>>(const istream&, IntegerSet&);
};

const ostream& operator<<(const ostream& osIntegerSet, const IntegerSet& integerSet) {
	cout << "number of elements : " << integerSet.numElements << endl;
	for (int i = 0; i < integerSet.numElements; i++) {
		cout << integerSet.elements[i] << " ";
	}
	cout << endl;

	return osIntegerSet;
}

int main() {
	int values[3] = { 3, 4, 5 };
	int values2[5] = { 1, 2, 3, 4, 5 };
	IntegerSet is1(10);
	IntegerSet is2(10);
	IntegerSet is3(10);

	/* Initialize IntegerSets */
	for (int i = 0; i < 3; i++) {
		is1.addAnElement(values[i]);
	}

	for (int i = 0; i < 5; i++) {
		is2.addAnElement(values2[i]);
	}

	cin >> is3;

	/* Superset Test */
	bool isSuperset = is1 < is2;
	if (isSuperset) {
		cout << "Superset" << endl;
	}
	else  {
		cout << "Not Superset" << endl;
	}

	isSuperset = is1 < is3;
	if (isSuperset) {
		cout << "Superset" << endl;
	}
	else  {
		cout << "Not Superset" << endl;
	}

	/* Equal Test */
	bool isEqual = (is1 == is2);
	if (isEqual) {
		cout << "Equal" << endl;
	}
	else {
		cout << "Not Equal" << endl;
	}

	is1 = is2;
	cout << is1;

	isEqual = (is1 == is2);
	if (isEqual) {
		cout << "Equal" << endl;
	}
	else {
		cout << "Not Equal" << endl;
	}

	/* operator[] Test */
	cout << "is1[3] = " << is1[3] << endl;

	return EXIT_SUCCESS;
}


