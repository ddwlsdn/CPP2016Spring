#include <iostream>

using namespace std;

const int SUCCESS = 0;
const int FAILURE = -1;
const int DUPLICATE = -2;

class IntegerSet {
	int numElements, maxNumElements;
	int *elements;

public:
	IntegerSet(const IntegerSet &other) {
		numElements = other.numElements;
		maxNumElements = other.maxNumElements;
		elements = new int[other.maxNumElements];
		
		for (int i = 0;i < other.numElements;i++) {
			elements[i] = other.elements[i];
		}
	};
	IntegerSet(int max) {
		numElements = 0;
		maxNumElements = max;
		elements = new int[max];
	}

	void sort();

	int addAnElement(int element); 
	int deleteAnElement(int element);
	int findAnElement(int element);
	int operator[](int position);

	IntegerSet operator=(const IntegerSet&);
	IntegerSet operator+(const IntegerSet&); 
	IntegerSet operator-(const IntegerSet&); 
	IntegerSet operator*(const IntegerSet&); 

	bool operator==(const IntegerSet&);
	bool operator<(const IntegerSet&);
	int operator~();

	friend istream& operator>>(const istream&, IntegerSet&);
	friend const ostream& operator<<(const ostream&, const IntegerSet&); // given as below

	~IntegerSet();
};

const ostream& operator<<(const ostream& osIntegerSet, const IntegerSet& integerSet) {
	cout << "number of elements : " << integerSet.numElements << endl;
	for (int i = 0; i < integerSet.numElements; i++) {
		cout << integerSet.elements[i] << " ";
	}
	cout << endl;

	return osIntegerSet;
}

IntegerSet::~IntegerSet() {
	delete(elements);
}

void IntegerSet::sort() {
	int temp;
	for (int i = 0;i < numElements;i++) {
		for (int j = i + 1;j < numElements;j++) {
			if (elements[i] > elements[j]) {
				temp = elements[i];
				elements[i] = elements[j];
				elements[j] = temp;
			}
		}
	}
}

int IntegerSet::addAnElement(int element) {
	if (numElements < maxNumElements) {
		if(findAnElement(element) == FAILURE){ 
			elements[numElements] = element;
			numElements++;
			return SUCCESS;
		}
		return DUPLICATE;
	}
	else
		return FAILURE;
}

int IntegerSet::deleteAnElement(int element) {
	int deleteIndex = findAnElement(element);
	if (deleteIndex == FAILURE) {
		return FAILURE;
	}
	else {
		for(int i = deleteIndex;i < numElements - 1;i++) {
			elements[i] = elements[i + 1]; 
		}
		numElements--;
	} 
		
}

int IntegerSet::findAnElement(int element) {
	for (int i = 0;i < numElements;i++) {
		if (elements[i] == element)
			return i;
	}

	return FAILURE;
}

IntegerSet IntegerSet::operator+(const IntegerSet& other) {
	int i;
	IntegerSet temp(numElements + other.numElements);//result of numElement is sum of this and other or smaller

	for (i = 0;i < numElements;i++) {
		temp.addAnElement(elements[i]);
	}

	for (i = 0;i < other.numElements;i++) {
		temp.addAnElement(other.elements[i]); //it will be add only if other's element is not in temp
	}
	return temp;
}

IntegerSet IntegerSet::operator-(const IntegerSet& other) {
	int i;
	IntegerSet temp(*this), temp_other(other);
	for (i = 0;i < temp_other.numElements;i++) {
		temp.deleteAnElement(temp_other.elements[i]); //it will be delete only if other's element is in temp
	}
	return temp;
}

IntegerSet IntegerSet::operator*(const IntegerSet& other) {
	int i;
	IntegerSet temp(numElements); //result of numElement is this or smaller
	for (i = 0;i < other.numElements;i++) {
		if (findAnElement(other.elements[i]) != FAILURE) { //if other's element is in this
			temp.addAnElement(other.elements[i]);
		}
	}
	return temp;
}

int IntegerSet::operator~() {
	IntegerSet temp(*this);
	temp.sort();
	return temp[numElements - 1];
};


istream& operator>>(istream& os, IntegerSet& other) {
	int input_Num, temp;

	cout << "Input the number of integers : ";
	cin >> input_Num;

	cout << "Input integer elements" << endl;
	for (int i = 0;i < input_Num;i++) {
		cin >> temp;
		other.addAnElement(temp);
	}
	return os;
}

bool IntegerSet::operator==(const IntegerSet& other) {
	if (numElements == other.numElements) {
		for (int i = 0;i < other.numElements; i++) { 
			if (!findAnElement(other.elements[i]) == FAILURE) 
				return false; //if one of other's element is not in this
		}
		return true; //if all of other's element is in this
	}
	return false;
}

IntegerSet IntegerSet::operator=(const IntegerSet& other) {
	numElements = other.numElements;
	maxNumElements = other.maxNumElements;

	int* temp = new int[other.maxNumElements];
	for (int i = 0;i < numElements;i++) {
		temp[i] = other.elements[i];
	}

	delete(elements);
	elements = temp;

	return *this;
}

int IntegerSet::operator[](int position) {
	position = position - 1;
	IntegerSet temp = *this;

	if (numElements < position)
		return - 1;
	else {
		temp.sort();
		return temp.elements[position];
	}
};
bool IntegerSet::operator<(const IntegerSet& other) {
	IntegerSet temp_other(other.maxNumElements); //because parmeter is const
	temp_other = other;

	if (numElements < other.numElements) {
		for (int i = 0;i < numElements; i++) {
			if (temp_other.findAnElement(elements[i]))
				return false;
		}
		return true;
	}
	return false;

}

int main() {
	int values[5] = { 1, 2, 3, 4, 5 };
	int values2[5] = { 5, 4, 3, 2, 1 };
	IntegerSet is1(10);
	IntegerSet is2(10);
	IntegerSet is3(10);
	
	/* Initialize IntegerSets */
	for (int i = 0; i < 5; i++) {
		is1.addAnElement(values[i]);
	}

	for (int i = 0; i < 5; i++) {
		is2.addAnElement(values2[i]);
	}

	cin >> is3;
	
	/* Superset Test */
	bool isSuperset = is1 < is2;
	if (isSuperset) {
		cout << "Superset" << endl;
	}
	else {
		cout << "Not Superset" << endl;
	}

	isSuperset = is1 < is3;
	if (isSuperset) {
		cout << "Superset" << endl;
	}
	else {
		cout << "Not Superset" << endl;
	}

	/* Equal Test */
	bool isEqual = (is1 == is2);
	if (isEqual) {
		cout << "Equal" << endl;
	}
	else {
		cout << "Not Equal" << endl;
	}

	is1 = is2;
	cout << is1;

	isEqual = (is1 == is3);
	if (isEqual) {
		cout << "Equal" << endl;
	}
	else {
		cout << "Not Equal" << endl;
	}

	/* operator[] Test */
	cout << "is1[3] = " << is1[3] << endl;
	int i,j,z;
	cin >> i >> j >> z ;
	cout << i << j << z <<endl;

	return EXIT_SUCCESS;
}
