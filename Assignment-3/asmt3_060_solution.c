#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct student{
	char name[10];
	int score;
	int grade;
} Student;

void init(FILE *fp, Student **student, int n) {
	/* initialize student by dynamic memory allocation  */
	*student = (Student *)malloc(sizeof(Student)* n);
}

int read(FILE *fp, Student *student, int n) {
	int i = 0;
	char name[10];
	int score;
	int grade;

	/* read i-th student data from fp */
	for (i = 0; i < n; i++) {
		if (fscanf(fp, "%s", name) == EOF) {
			return i;
		}
		if (fscanf(fp, "%d", &score) == EOF) {
			return i;
		}
		if (fscanf(fp, "%d", &grade) == EOF) {
			return i;
		}

		strcpy((student + i)->name, name);
		(student + i)->score = score;
		(student + i)->grade = grade;
	}

	return n;
}

void swap(Student *s1, Student *s2) {
	char tempStr[10];
	int temp;

	strcpy(tempStr, s1->name);
	strcpy(s1->name, s2->name);
	strcpy(s2->name, tempStr);

	temp = s1->score;
	s1->score = s2->score;
	s2->score = temp;

	temp = s1->grade;
	s1->grade = s2->grade;
	s2->grade = temp;
}

void sort(Student *student, int n) {
	int i = 0;
	int j = 0;
	Student temp;

	/* sort student by using bubble sort */
	for (i = 0; i < n; i++) {
		for (j = 0; j < n - i; j++) {
			if (strcmp((student + j)->name, (student + j + 1)->name) > 0) {
				swap((student + j), (student + j + 1));
			}
		}
	}
}

int write(FILE *fp, Student *student, int n) {
	int i = 0;

	/* write i-th student data to fp */
	for (i = 0; i < n; i++) {
		fprintf(fp, "%s %d %d\n", (student + i)->name, (student + i)->score, (student + i)->grade);
	}

	return n;
}

int main() {
	FILE *read_fp = fopen("data_60.txt", "r");
	FILE *write_fp = fopen("result.txt", "w");
	Student *student;
	int n;

	/* read size of array from read_fp */
	fscanf(read_fp, "%d", &n);

	/* initialize student pointer */
	init(read_fp, &student, n);

	/* read students data from read_fp */
	read(read_fp, student, n);

	/* sort students by name */
	sort(student, n);

	/* write students data to write_fp */
	write(write_fp, student, n);

	free(student);
	fclose(read_fp);
	fclose(write_fp);

	return EXIT_SUCCESS;
}