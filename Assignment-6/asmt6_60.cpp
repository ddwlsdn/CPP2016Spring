#include <fstream>
#include <iostream>
#include <string>

using namespace std;

class Student;

class Department{
private:
	char* name;
	int maxStudents, numStudents;
	Student *students;
public:
	Department();
	Department(char *name, int maxStudents);
	~Department();
	void AddAStudent(Student *);
	void sortStudents(bool ascend);
	void PrintDepartment();
};

class Student {
	char* name;
	int year;
	int ID;
public:
	Student(char* _name, int _year, int _ID);
};

Student::Student(char* _name, int _year, int _ID) {
	int len = strlen(_name);

	name = new char[len + 1];
	strcpy(name, _name);

	year = _year;

	ID = _ID;
}

int main() {
	Department cse("CSE", 10);

	Student stud_maldong("Maldong", 23, 201322332);
	Student stud_jinsoo("Jinsoo", 21, 201599999);
	Student stud_soomin("Soomin", 24, 201211231);

	cse.PrintDepartment();

	cse.AddAStudent(&stud_maldong);
	cse.AddAStudent(&stud_soomin);
	cse.AddAStudent(&stud_jinsoo);

	cse.sortStudents(true);
	cse.PrintDepartment();

	return 0;
}
