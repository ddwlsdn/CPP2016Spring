#include <iostream>

using namespace std;

class DistanceHelper {
public:
	float getDistance(Rectangle a, Rectangle b);
	float getDistance(Rectangle a, Line b);
	float getDistance(Line a, Line b);
	float getDistance(Line a, Point b);
	float getDistance(Point a, Point b);
};

class Point {
private:
	float x;
	float y;
public:
	Point();
	Point(float x, float y);
	float getX() const;
	float getY() const;
	friend ostream& operator<<(ostream&, const Point&);
};

class Rectangle;

class Line {
private:
	Point p1;
	Point p2;
public:
	Line();
	Line(Point, Point);
	float getLength();
	Point getP1() const;
	Point getP2() const;
	Point operator*(const Line&);
	Rectangle operator*(const Rectangle&);
	friend ostream& operator<<(ostream&, const Line&);
};

class Rectangle {
private:
	Point lower;
	Point upper;
public:
	Rectangle();
	Rectangle(const Point, const Point);
	Point getLower() const;
	Point getUpper() const;
	float getLength();
	Line operator*(const Line&);
	Rectangle operator*(const Rectangle&);
	friend ostream& operator<<(ostream&, const Rectangle&);
};

int main(void) {

	Point p1(0, 3);
	Point p2(4, 1);
	Line l1(p1, p2);

	Point p3(-1, -1);
	Point p4(4, 4);
	Line l2(p3, p4);

	Point p5(-1, 1);
	Point p6(-1, 5);
	Line l3(p5, p6);

	Point p7(1, 1);
	Point p8(3, 4);
	Rectangle r1(p7, p8);

	Point p9(-2, -4);
	Point p10(2.5f, 3.1f);
	Rectangle r2(p9, p10);

	Point p11(-1, -1);
	Point p12(0, 0);
	Rectangle r3(p11, p12);

	// test intersection between lines
	Point xP1 = l1 * l2;
	cout << "=== xP1 ===" << endl << xP1; // 2, 2

	Point xP2 = l1 * l3;
	cout << "=== xP2 ===" << endl << xP2; // 교차 x

										  // test rectangle intersecting with line
	Line xL1 = r1 * l1;
	cout << "=== xL1 ===" << endl << xL1; // (1, 2.5) (3, 1.5)

	Line xL2 = r1 * l3;
	cout << "=== xL2 ===" << endl << xL2; // 교차 x

										  // test line intersecting with rectangle
	Rectangle xR1 = l1 * r1;
	cout << "=== xR1 ===" << endl << xR1; // (1, 1.5) (3, 2.5)

	Rectangle xR2 = l3 * r1;
	cout << "=== xR2 ===" << endl << xR2; // 교차 x

										  // test intersection between rectangles
	Rectangle xR3 = r1 * r2;
	cout << "=== xR3 ===" << endl << xR3; // (1, 1) (2.5f, 3.1f)
	cout << xR3;

	DistanceHelper helper;

	// test distance between rectangles
	float dstRR1 = helper.getDistance(r1, r3); // sqrt(2)
	float dstRR2 = helper.getDistance(r1, r2); // 교차 => 0.0

											   // test distance between line and rectangle
	float dstRL1 = helper.getDistance(r1, l3); // 2
	float dstRL2 = helper.getDistance(l3, r1); // 2

	float dstRL3 = helper.getDistance(l1, r1); // 교차 => 0.0
	float dstRL4 = helper.getDistance(r1, l1); // 교차 => 0.0

											   // test distance between lines
	float dstLL1 = helper.getDistance(l1, l2); // 교차 => 0.0
	float dstLL2 = helper.getDistance(l2, l3); // sqrt(2)

	cout << "test Distance => " << endl;
	cout << dstRR1 << "," << dstRR2 << endl
		<< dstRL1 << "," << dstRL2 << endl
		<< dstRL3 << "," << dstRL4 << endl
		<< dstLL1 << "," << dstLL2 << endl;

	// test Length
	float lengthofRec = r1.getLength();
	float lengthofLine = l2.getLength();

	cout << "test Length => " << endl;
	cout << lengthofRec << "," << lengthofLine << endl; // 10, sqrt(50)

	return EXIT_SUCCESS;
}