#include <iostream>
#include <cmath>
#include <vector>
#include <algorithm>
#include <limits> 

using namespace std;

#define INFINITE std::numeric_limits<double>::infinity()
#define EPSILON 1e-8
#define UNDEFIND -1


class Point {
    double x, y;

public:
    Point() {}
    Point(double x, double y) {
        this->x = x;
        this->y = y;
    }
    double distance(const Point& o) {
        return sqrt(pow((this->x - o.x), 2) + pow((this->y - o.y), 2));
    }
    double getX() {
        return x;
    }
    double getY() {
        return y;
    }
	void setX(double _x) {
		x = _x;
	}
	void setY(double _y) {
		y = _y;
	}
	bool operator==(Point& p) {
		if(x == p.getX() && y == p.getY()) {
			return true;
		}
		return false;
	}
    friend ostream& operator<<(ostream& os, const Point& p);
};

ostream& operator<<(ostream& os, const Point& p) {
    os << "(" << p.x << " , " << p.y << ")";
    return os;
}

class Line {
	Point start, end;

public:
    Line() {}
    Line(Point _start, Point _end) {
		Line temp(_start.getX(), _start.getY(), _end.getX(), _end.getY());
		start = temp.start;
		end = temp.end;
    }
    Line(double x1, double y1, double x2, double y2) {
       if (x1 < x2) {
            this->start = Point(x1,y1);
            this->end = Point(x2,y2);
        }
        else if (x1 > x2) {
            this->start = Point(x2,y2);
            this->end = Point(x1,y1);
        }
        else { // x1 == x2
            if (y1 < y2) {
                this->start = Point(x1,y1);
                this->end = Point(x2,y2);
            }
            else {
                this->start = Point(x2,y2);
                this->end = Point(x1,y1);
            }
        }
    }
	Point getStartPoint(){
		return start;
	}
	Point getEndPoint(){
		return end;
	}
    double getLength() {
        return start.distance(end);
    }
    double getSlope() {
        if (start.getX() == end.getX()) return INFINITE;
        return (start.getY() - end.getY()) / (start.getX() - end.getX());
    }
    bool inRect(Point p) {
        double minx = min(start.getX(), end.getX());
        double maxx = max(start.getX(), end.getX());
        double miny = min(start.getY(), end.getY());
        double maxy = max(start.getY(), end.getY());
        if (minx <= p.getX() && maxx >= p.getX() && miny <= p.getY() && maxy >= p.getY()) return true;
        return false;
    }
    Point getIntersection(Line& o) {
        double slope1 = getSlope();
        double slope2 = o.getSlope();
        double x, y;
		if (slope1 == slope2) return Point(UNDEFIND, UNDEFIND);
        if (slope1 == INFINITE) {
            x = start.getX();
            y = x * slope2 + (o.start.getY() - o.start.getX() * slope2); //y = a(x - x') + y' , x = constant => result = a(constant - x') + y'
        }
        else if (slope2 == INFINITE) {
            x = o.start.getX();
            y = x * slope1 + (start.getY() - start.getX() * slope1);//y = a(x - x') + y' , x = constant => result = a(constant - x') + y'
        }
        else {// a1(x - x') + y' = a2(x - x'') + y'' => (a1 - a2)x = (a1x' - y') - (a2x'' - y'')
            double c1 = start.getX() * slope1 - start.getY(); //(a1x' - y')
            double c2 = o.start.getX() * slope2 - o.start.getY();// (a2x'' - y'')
            double ss = slope1 - slope2;//(a1 - a2)
            double cc = c1 - c2;//(a1x' - y') - (a2x'' - y'')
            x = cc / ss;
            y = x * slope1 - c1;// = a1(x - x') + y' => a1x - (a1x' - y')
        }
		Point t(x, y);
        if (inRect(t) && o.inRect(t)) {
            return t;
        }
        return Point(UNDEFIND, UNDEFIND);
    }
    Point operator*(Line& o) {
        return getIntersection(o);
    }
    double distance(Point p) {
        Line l = *this;
        double px = p.getX();
        double py = p.getY();
        double startX = l.start.getX();
        double startY = l.start.getY();
        double endX = l.end.getX();
        double endY = l.end.getY();

        double startToPoint_x = px - startX;//start->point vector.x
        double startToPoint_y = py - startY;//start->point vector.y
        double startToEnd_x = endX - startX;//start->end vector.x
        double startToEnd_y = endY - startY;//start->end vector.y

        double dot = startToPoint_x * startToEnd_x + startToPoint_y * startToEnd_y;//inner product 
        double squareLineLength = pow(startToEnd_x, 2) + pow(startToEnd_y,2);//(line length)^2
        double footOfPerpendicularToLineLengthRatio= -1;
        if (squareLineLength != 0)
            footOfPerpendicularToLineLengthRatio = dot / squareLineLength;

        double nearest_x, nearest_y;

        if (footOfPerpendicularToLineLengthRatio < 0) { //point exist on left of line
            nearest_x = startX;
            nearest_y = startY;
        }
        else if (footOfPerpendicularToLineLengthRatio > 1) {//point exist on right of line
            nearest_x = endX;
            nearest_y = endY;
        }
        else {
            nearest_x = startX + footOfPerpendicularToLineLengthRatio * startToEnd_x;//coordinate of foot of perpendicular
            nearest_y = startY + footOfPerpendicularToLineLengthRatio * startToEnd_y;
        }

        double dx = px - nearest_x;
        double dy = py - nearest_y;
        return sqrt(dx * dx + dy * dy);
    }
    double distance(Line l) {
        Line self = *this;
        double ret = INFINITE;
        Point c = self.getIntersection(l);
		if (c.getX() != UNDEFIND) return 0; //they intersact each other
        ret = min(ret, l.distance(self.start));//minimal distance among distance of each Line to the other edge point 
        ret = min(ret, l.distance(self.end));
        ret = min(ret, self.distance(l.start));
        ret = min(ret, self.distance(l.end));
        return ret;
    }
	
    friend ostream& operator<<(ostream& os, const Line& l);
};

ostream& operator<<(ostream& os, const Line& l) {
	os << l.start  << " / " << l.end << endl;
    return os;
}

class Rect {
	Line lines[4];//clockwise
	void setRect(double x1, double y1, double x2, double y2) {//to avoid duplicate of code
		Point leftBottom = Point(min(x1, x2), min(y1, y2));
		Point rightUp = Point(max(x1, x2), max(y1, y2));
		lines[0] = Line(leftBottom.getX(), leftBottom.getY(), rightUp.getX(), leftBottom.getY());
		lines[1] = Line(leftBottom.getX(), leftBottom.getY(), leftBottom.getX(), rightUp.getY());
		lines[2] = Line(leftBottom.getX(), rightUp.getY(), rightUp.getX(), rightUp.getY());
		lines[3] = Line(rightUp.getX(), leftBottom.getY(), rightUp.getX(), rightUp.getY());
	}
public:
    Rect() {}
    Rect(Point o1, Point o2) {
        double x1 = o1.getX();
        double y1 = o1.getY();
        double x2 = o2.getX();
        double y2 = o2.getY();
		setRect(x1, y1, x2, y2);
    }
	Line getLine(int i) {
		return lines[i];
	}
	Rect(double x1, double y1, double x2, double y2) {
		setRect(x1, y1, x2, y2);
	}
    double getLength() {
		return (lines[0].getLength()) * 2 + (lines[1].getLength()) * 2;
    }
    double getArea() {
        return (lines[0].getLength()) * (lines[1].getLength());
    }
    Rect operator*(Rect& rect) {
        Point lb( INFINITE, INFINITE );
        Point ru( -INFINITE, -INFINITE );
		Rect null(-1,-1,-1,-1);
        for (int i = 0; i < 4; i++) {//check intersection between all of lines in each rectagle
            for (int j = 0; j < 4; j++) {
                Point its = lines[i].getIntersection(rect.lines[j]);
				if (its.getX() != UNDEFIND) {
                    lb.setX(min(lb.getX(), its.getX()));//save minimal intersection
                    lb.setY(min(lb.getY(), its.getY()));
                    ru.setX(max(ru.getX(), its.getX()));//save maximal intersection
                    ru.setY(max(ru.getY(), its.getY()));
                }
            }
        }
		Rect r( lb , ru );
		if(r == null) {
			return Rect( -1, -1, -1, -1 );
		} 
        if (r.getArea() > EPSILON) return r;
        else return Rect( -1, -1, -1, -1 );
        
    }
	Line operator*(Line& line) {
		Line null = Line( -1, -1, -1, -1 );
		vector<Point> v;
		for (int i = 0; i < 4; i++) {
			Line l = lines[i];
			Point p = l.getIntersection(line);
			if (p.getX() != UNDEFIND)
				v.push_back(p);
		}
		if (v.size() < 2) return null;
		return Line(v.front(), v.back());

	}
	bool operator==(Rect& rect) {
		if(lines[0].getStartPoint() == rect.getLine(0).getStartPoint() && lines[2].getEndPoint() == rect.getLine(2).getEndPoint()) {
			return true;	
		}
		return false;
	}
    double distance(Rect o) {
        double distance = INFINITE;
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                distance = min(distance, lines[i].distance(o.lines[j]));
            }
        }
        return distance;
    }
    double distance(Line o) {
        double distance = INFINITE;
        for (int i = 0; i < 4; i++)
            distance = min(distance, lines[i].distance(o));
        return distance;
    }
    friend ostream& operator<<(ostream& os, const Rect& l);
};

ostream& operator<<(ostream& os, const Rect& l) {
	Line bottom = l.lines[0];
	Line up = l.lines[2];
	os << bottom.getStartPoint() <<" // " << up.getEndPoint() << endl;
    return os;
}




Rect operator*(Line& line, Rect& rect) {
    Rect null = Rect( -1, -1, -1, -1 );
    int its = 0;
    vector<Point> v;
    for (int i = 0; i < 4; i++) {
		Line l = rect.getLine(i);
        Point p = l.getIntersection(line);
		if (p.getX() != UNDEFIND)
            v.push_back(p);
    }
    if (v.size() < 2) return null;
	Point leftBottom(min(v[0].getX(), v[1].getX()), min(v[0].getY(), v[1].getY()));
	Point rightUp(max(v[0].getX(), v[1].getX()), max(v[0].getY(), v[1].getY()));
	Rect result(leftBottom, rightUp);
	if(result.getArea() > EPSILON) return result;
	else return null;//not rectagle, just line
}

class RectSet {
public:
    Rect **rects;
    int num, max;
    RectSet(int maxSize) {
        max = maxSize;
        rects = new Rect*[maxSize];
		num = 0;
    }
	RectSet(RectSet& rs) {
		max = rs.max;
        rects = new Rect*[max];
		for(int i = 0; i < rs.num; i++) {
			rects[i] = rs.rects[i];
		}
		num = rs.num;
    }
    void add(Rect& r) {
        if (max <= num) return;
		Rect *temp = new Rect(r);
        rects[num] = temp;
		num++;
    }
    RectSet operator*(Line& l) {
		Rect null(-1,-1,-1,-1);
        RectSet retSet = RectSet(max);
        for (int i = 0; i < num; i++) {
            Rect r = l * (*rects[i]);
			if (r == null) continue; //this rectagle doesn't meet with line
            retSet.add(r);
        }
        return retSet;
    }
    friend ostream& operator<<(ostream& os, const RectSet& rs);
	~RectSet() {
		delete[] rects;
	}
	
};

ostream& operator<<(ostream& os, const RectSet& rs) {
    for (int i = 0; i < rs.num; i++) {
        Rect l = *(rs.rects[i]);
        os << l << endl;
    }
    return os;
}

class DistanceHelper {
public:
    double getDistance(Rect a, Rect b) {
        return a.distance(b);
    }
    double getDistance(Rect a, Line b) {
        return a.distance(b);
    }
    double getDistance(Line a, Rect b) {
        return b.distance(a);
    }
    double getDistance(Line a, Line b) {
        return a.distance(b);
    }
    double getDistance(Line a, Point b) {
        return a.distance(b);
    }
    double getDistance(Point a, Point b) {
        return a.distance(b);
    }
};

int main(void) {
	Point p1(0, 3);
	Point p2(4, 1);
	Line l1(p1, p2);

	Point p3(-1, -1);
	Point p4(4, 4);
	Line l2(p3, p4);

	Point p5(-1, 1);
	Point p6(-1, 5);
	Line l3(p5, p6);

	Point p7(1, 1);
	Point p8(3, 4);
	Rect r1(p7, p8);

	Point p9(-2, -4);
	Point p10(2.5f, 3.1f);
	Rect r2(p9, p10);

	Point p11(-1, -1);
	Point p12(0, 0);
	Rect r3(p11, p12);

	// test intersection between lines
	Point xP1 = l1 * l2;
	cout << "=== xP1 ===" << endl << xP1 << endl; // 2, 2

	Point xP2 = l1 * l3;
	cout << "=== xP2 ===" << endl << xP2 << endl; // 교차 x

										  // test rectangle intersecting with line
	Line xL1 = r1 * l1;
	cout << "=== xL1 ===" << endl << xL1; // (1, 2.5) (3, 1.5)

	Line xL2 = r1 * l3;
	cout << "=== xL2 ===" << endl << xL2; // 교차 x

										  // test line intersecting with rectangle
	Rect xR1 = l1 * r1;
	cout << "=== xR1 ===" << endl << xR1; // (1, 1.5) (3, 2.5)

	Rect xR2 = l3 * r1;
	cout << "=== xR2 ===" << endl << xR2; // 교차 x

										  // test intersection between rectangles
	Rect xR3 = r1 * r2;
	cout << "=== xR3 ===" << endl << xR3; // (1, 1) (2.5f, 3.1f)

	DistanceHelper helper;

	// test distance between rectangles
	float dstRR1 = helper.getDistance(r1, r3); // sqrt(2)
	float dstRR2 = helper.getDistance(r1, r2); // 교차 => 0.0

											   // test distance between line and rectangle
	float dstRL1 = helper.getDistance(r1, l3); // 2
	float dstRL2 = helper.getDistance(l3, r1); // 2

	float dstRL3 = helper.getDistance(l1, r1); // 교차 => 0.0
	float dstRL4 = helper.getDistance(r1, l1); // 교차 => 0.0

											   // test distance between lines
	float dstLL1 = helper.getDistance(l1, l2); // 교차 => 0.0
	float dstLL2 = helper.getDistance(l2, l3); // sqrt(2)

	cout << "test Distance => " << endl;
	cout << dstRR1 << "," << dstRR2 << endl
		<< dstRL1 << "," << dstRL2 << endl
		<< dstRL3 << "," << dstRL4 << endl
		<< dstLL1 << "," << dstLL2 << endl;

	// test Length
	float lengthofRec = r1.getLength();
	float lengthofLine = l2.getLength();

	cout << "test Length => " << endl;
	cout << lengthofRec << "," << lengthofLine << endl; // 10, sqrt(50)

    RectSet rectangleSet(10);
    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 5; j++) {
            Point lower(j, i);
            Point upper(j + 1, i + 1);
            Rect rectangle(lower, upper);

            rectangleSet.add(rectangle);
        }
    }
    cout << rectangleSet;

    Point pp1(-1, -0.5);
    Point pp2(6, 0.5);
    Line line1(pp1, pp2);
	
    RectSet intersectionSet1 = rectangleSet * line1;
    cout << "IntersectionSet1 :" << endl;
    cout << intersectionSet1;

    Point pp3(-1, 1.5);
    Point pp4(6, 1.5);
    Line line2(pp3, pp4);  

    RectSet intersectionSet2 = rectangleSet * line2;
    cout << "IntersectionSet2 : " << endl;
    cout << intersectionSet2;
    return EXIT_SUCCESS;
}